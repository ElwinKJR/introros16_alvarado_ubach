#include "ros/ros.h"
#include "intros_services_example_sergi/sum_sergi.h"
#include "std_msgs/String.h"

bool add(intros_services_example_sergi::sum_sergi::Request &req, intros_services_example_sergi::sum_sergi::Response &res)
{
	res.sum = req.a + req.b;
	ROS_INFO("request: x=%ld, y=%ld",(long int)req.a,(long int)req.b);
	ROS_INFO("sending back response: [%ld]",(long int)res.sum);
	return true;
}


int main(int arg, char **argv)
{
	std_msgs::String msg;

	ros::init(arg, argv, "service_server");
	ros::NodeHandle nh;
	ros::Rate loop_rate(10);

	ros::ServiceServer service = nh.advertiseService("sum_sergi",add);
	ROS_INFO("Ready to add two ints!");

	while(ros::ok())
	{
		ROS_INFO("Iteration service start");
		
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
