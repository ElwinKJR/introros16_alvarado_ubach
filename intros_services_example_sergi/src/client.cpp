#include "ros/ros.h"
#include "intros_services_example_sergi/sum_sergi.h"
#include <cstdlib>
#include "std_msgs/String.h"



int main(int arg, char **argv)
{
	ros::init(arg, argv, "service_client");
	
	if(arg != 3)
	{
		ROS_INFO("usage: add_two_ints_client X Y");
   		return 1;
	}

	ros::NodeHandle n;

	ros::ServiceClient client = n.serviceClient<intros_services_example_sergi::sum_sergi>("sum_sergi");
	intros_services_example_sergi::sum_sergi srv;
	srv.request.a = atoll(argv[1]);
	srv.request.b = atoll(argv[2]);
	
	if(client.call(srv)){
		ROS_INFO("Sum: %ld", (long int)srv.response.sum);
	}else{
    		ROS_ERROR("Failed to call service add_two_ints");
    		return 1;
  	}
	
	return 0;
}
