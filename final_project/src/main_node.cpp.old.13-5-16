#include "ros/ros.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include "geometry_msgs/Twist.h"
#include <stdio.h>
#include "ar_pose/ARMarker.h"
#include <dynamic_reconfigure/server.h>
#include <final_project/dynparamsConfig.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/Range.h>
#include <rosgraph_msgs/Clock.h>

#define pi 3.14159265

float marker_X_floor, marker_Y_floor, marker_Z_floor;
float marker_X_front, marker_Y_front, marker_Z_front;
bool marker_Take_off, marker_Land;
//Dynamic reconfigure
float desired_X, desired_Y, desired_Rot_Z;
bool desired_Take_off, desired_Land, desired_camera;

bool front_marker_found=false;

double roll_floor, pitch_floor, yaw_floor;
double roll_front, pitch_front=5, yaw_front;
double speed = 0.1, sonar_Z;
ros::Time ros_time, ros_time_zero;

std_msgs::Empty msg;
geometry_msgs::Twist msg_move;	

int state;
double error_linear = 0.01, error_angular = 0.01;
double kp = 0.01;	

// TODO: when localizing the marker make sure to be in front of it and not just faceing it (maybe trigonometri with the angles?)

void dynamic_callback(final_project::dynparamsConfig &config, uint32_t level) 
{
	desired_Take_off = config.Take_off;
	desired_Land = config.Land;
	desired_X = config.Xd;
	desired_Y = config.Yd;
	desired_Rot_Z = config.Rot_Z;
	desired_camera = config.Toggle_Camera;
	config.Toggle_Camera = false;
	ROS_INFO("Callback dynamic reconfigure");
}

void pose_front_Callback(const ar_pose::ARMarker& msg)
{	
	marker_X_front = msg.pose.pose.position.x;
	marker_Y_front = msg.pose.pose.position.y;
	marker_Z_front = msg.pose.pose.position.z;

	tf::Quaternion q(msg.pose.pose.orientation.x, 
			msg.pose.pose.orientation.y, 
			msg.pose.pose.orientation.z, 
			msg.pose.pose.orientation.w);
	tf::Matrix3x3 m(q);
	m.getRPY(roll_front,pitch_front,yaw_front);
	ROS_INFO("Callback pose front");
}

void pose_floor_Callback(const ar_pose::ARMarker& msg)
{	
	marker_X_floor = msg.pose.pose.position.x;
	marker_Y_floor = msg.pose.pose.position.y;
	marker_Z_floor = msg.pose.pose.position.z;

	tf::Quaternion q(msg.pose.pose.orientation.x, 
			msg.pose.pose.orientation.y, 
			msg.pose.pose.orientation.z, 
			msg.pose.pose.orientation.w);
	tf::Matrix3x3 m(q);
	m.getRPY(roll_floor,pitch_floor,yaw_floor);
	ROS_INFO("Callback pose floor");
}

void sonar_Callback(const sensor_msgs::Range& msg)
{	
	sonar_Z = msg.range;
	ROS_INFO("Callback sonar");
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "take_off_publisher");
	ros::NodeHandle n;
	ros::Rate loop_rate(10);


	ROS_INFO("MAIN STARTING!!!");

	ros::Publisher take_off_pub = n.advertise<std_msgs::Empty>("/ardrone/takeoff", 50);
	ros::Publisher land_pub = n.advertise<std_msgs::Empty>("/ardrone/land", 50);
	ros::Publisher move_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);

	dynamic_reconfigure::Server<final_project::dynparamsConfig> server;
	dynamic_reconfigure::Server<final_project::dynparamsConfig>::CallbackType f;
	f = boost::bind(&dynamic_callback, _1, _2);
	server.setCallback(f);

	ros::ServiceClient camera = n.serviceClient <std_srvs::Empty> ("/ardrone/togglecam");
	std_srvs::Empty camera_srv;

	ros::Subscriber sub_ar_pose_front = n.subscribe("/ar_pose_marker_front", sizeof(ar_pose::ARMarker), pose_front_Callback);
	ros::Subscriber sub_ar_pose_floor = n.subscribe("/ar_pose_marker_floor", sizeof(ar_pose::ARMarker), pose_floor_Callback);
	ros::Subscriber sub_sonar = n.subscribe("/sonar_height", sizeof(sensor_msgs::Range), sonar_Callback);


	ros::Duration d = ros::Duration(3, 0);
	d.sleep();
	ros_time_zero = ros::Time::now();
	
	state = -1;
	ROS_INFO("STATE 0: Take off");
  	while (ros::ok())
  	{

		if (state == -1)
		{
			take_off_pub.publish(msg);
			camera.call(camera_srv);
	
			state = 0;
			ROS_INFO("STATE 0: Wait for take off");
		}
		if (state == 0)
		{
			ros_time = ros::Time::now();
			if ((ros_time.sec-ros_time_zero.sec)>=7){
				state = 1;
				ROS_INFO("STATE 1: Reach 1 meter from floor");
			}
		}
		if (state == 1)
		{			
			msg_move.linear.y = -kp*marker_X_floor;
			msg_move.linear.x = -kp*marker_Y_floor;
			msg_move.linear.z = kp*(1 - marker_Z_floor);
			move_pub.publish(msg_move);

			ROS_INFO("marker floor Z: %f",marker_Z_floor);

			if (msg_move.linear.z*msg_move.linear.z < error_linear)
			{
				state = 2;
				ROS_INFO("STATE 2: Stabilize over floor marker");
				ros_time_zero = ros::Time::now();
			}	
		}
		else if (state == 2)
		{
			ros_time = ros::Time::now();
			if ((ros_time.sec-ros_time_zero.sec)>=5){
				state = 3;
				ROS_INFO("STATE 3: Toggle Camera");
			}
		
			//if (yaw_floor>0 && yaw_floor <pi)	msg_move.angular.z = -0.1;
			//if (yaw_floor<0 && yaw_floor >-pi)	msg_move.angular.z = 0.1;
	
			msg_move.linear.y = -kp*marker_X_floor;
			msg_move.linear.x = -kp*marker_Y_floor;
			msg_move.linear.z = kp*(1 - marker_Z_floor);

			move_pub.publish(msg_move);
		}
		else if (state == 3)
		{
			camera.call(camera_srv);
	
			msg_move.linear.y = 0;
			msg_move.linear.x = 0;
			msg_move.linear.z = 0;	
			msg_move.angular.z = 0;

			state = 4;
			ROS_INFO("STATE 4: Look for front marker");
		}
		else if (state == 4)
		{		
			if (pitch_front<error_angular && pitch_front>-error_angular){
				state = 5;	//front marker found
				ROS_INFO("STATE 5: Navigate to front marker");
			}

			msg_move.angular.z = 0.3;

			move_pub.publish(msg_move);
		}
		else if (state == 5)
		{			
			msg_move.angular.z = -0.05*pitch_front;
	
			msg_move.linear.x = kp*(marker_Z_front - 1);
			msg_move.linear.y = -kp*marker_X_front;
			msg_move.linear.z = kp*(1 - marker_Y_front);

			move_pub.publish(msg_move);

			if (msg_move.linear.x*msg_move.linear.x <error_linear &&
			    msg_move.linear.y*msg_move.linear.y <error_linear && 
			    msg_move.linear.z*msg_move.linear.z <error_linear){
				state = 6;
				ROS_INFO("STATE 6: Wait 5 seconds");
				ros_time_zero = ros::Time::now();
			}
		}
		else if (state == 6)
		{
			ros_time = ros::Time::now();	
			if ((ros_time.sec-ros_time_zero.sec)>=5){
				state = 7;
				ROS_INFO("STATE 7: Land");
			}
			
			msg_move.angular.z = -0.05*pitch_front;
	
			msg_move.linear.x = kp*(marker_Z_front - 1);
			msg_move.linear.y = -kp*marker_X_front;
			msg_move.linear.z = kp*(1 - marker_Y_front);

			move_pub.publish(msg_move);
		}
		else if (state == 7)
		{
			land_pub.publish(msg);

			state = 8;
			ROS_INFO("STATE 8: Task finished");
		}

		ros::spinOnce();
		loop_rate.sleep();
	}				
  	return 0;
}
