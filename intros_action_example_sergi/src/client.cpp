#include "ros/ros.h"
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <intros_action_example_sergi/FibonacciAction.h>


int main(int arg, char **argv)
{
	ros::init(arg, argv, "action_client");

	ros::NodeHandle n;

	// create the action client. true causes the client to spin its own thread
	actionlib::SimpleActionClient<intros_action_example_sergi::FibonacciAction> ac("action_server/fibonacci", true);

	ROS_INFO("Waiting for action server to start.");
	// wait for the action server to start
	ac.waitForServer();
	ROS_INFO("Action client node started.");

	int count = 1;
	while (ros::ok())
	{
	 	ROS_INFO("Action server started, sending goal.");
		intros_action_example_sergi::FibonacciGoal goal;
		goal.order = count;
		//reset seed and send goal
		if (count==20) count=0;
		ac.sendGoal(goal);

		//wait for the action to return
		bool finished_before_timeout = ac.waitForResult(ros::Duration(30.0));
		if (finished_before_timeout) {
		    actionlib::SimpleClientGoalState state = ac.getState();
		    ROS_INFO("Action finished: %s", state.toString().c_str());
		    std::cout << *(ac.getResult()) << std::endl;
		} else {
				ROS_INFO("Action did not finish before the time out.");
		}

        ++count;
		ros::spinOnce();
	}
}









