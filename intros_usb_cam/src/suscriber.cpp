#include "ros/ros.h"
#include "std_msgs/String.h"
#include "ar_pose/ARMarker.h"
#include <dynamic_reconfigure/server.h>
#include <intros_usb_cam/dynparamsConfig.h>
#include <math.h>


float current_X, current_Y, current_Z;
float desired_X, desired_Y, desired_Z;


void callback(intros_usb_cam::dynparamsConfig &config, uint32_t level) {
	desired_X = config.Xd;
	desired_Y = config.Yd;
	desired_Z = config.Zd;
}

void poseCallback(const ar_pose::ARMarker& msg)
{	
	current_X =  msg.pose.pose.position.x;
	current_Y =  msg.pose.pose.position.y;
	current_Z =  msg.pose.pose.position.z;
}

int main(int arg, char **argv)
{
	ros::init(arg, argv, "subscriber_intro_node");
	ros::NodeHandle n;
	ros::Rate loop_rate(0.5);

	ros::Subscriber sub = n.subscribe("ar_pose_marker", 1000, poseCallback);

	dynamic_reconfigure::Server<intros_usb_cam::dynparamsConfig> server;
	dynamic_reconfigure::Server<intros_usb_cam::dynparamsConfig>::CallbackType f;
	f = boost::bind(&callback, _1, _2);
	server.setCallback(f);

	while(ros::ok())
	{
		ROS_INFO("Iteration Subscriber start");
  		ROS_INFO("Desired position: x=%f y=%f z=%f", desired_X, desired_Y, desired_Z);
		ROS_INFO("Current position: x=%f y=%f z=%f", current_X, current_Y, current_Z);
		ROS_INFO("ERROR:%f",sqrt(
				(desired_X - current_X)*(desired_X - current_X)+
				(desired_Y - current_Y)*(desired_Y - current_Y)+
				(desired_Z - current_Z)*(desired_Z - current_Z)));

		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
